const btnNovaTrivia = document.querySelector("#btnNovaTrivia");
const btnFinalizarTrivia = document.querySelector("#btnFinalizarTrivia");
const questoes = document.querySelector("#questoes");
const mensagem = document.querySelector("#mensagem");


let todasQuestoes;

function gerarNovaTrivia(){
    let url = "https://opentdb.com/api.php?amount=1&category=27&difficulty=easy&type=boolean";
    fetch(url).then(extrairJson).then(iniciarTrivia);
}

function extrairJson(resposta){
    return resposta.json();
}

function finalizarTrivia(){

    let perguntasRespondidas = document.querySelectorAll("#perguntas")[0];

    console.log(perguntasRespondidas.innerHTML);

    for(let questao of todasQuestoes.results)
    {

        if(perguntasRespondidas.children[0].innerHTML == questao.question){

            //TRUE
            if(Boolean(perguntasRespondidas.children[1].checked) == Boolean(questao.correct_answer)){
                mensagem.innerHTML = "RESPOSTA CORRETA.";

            } else{
                mensagem.innerHTML = "RESPOSTA INCORRETA.";
            }
          
        } 
    }

}


function iniciarTrivia(dados){

let cont = 0;

    if (dados.response_code == 0)
    {
       todasQuestoes = dados;

       for(let questao of dados.results){
            let sessaoPerg = document.createElement("section");
            let labelPerg =document.createElement("label");
            let radioButtonYes = document.createElement("input");
            let radioButtonNo = document.createElement("input");

            radioButtonNo.innerText = "Yes";
            radioButtonYes.innerText = "No";
            
            radioButtonYes.type = radioButtonNo.type = "radio";
            radioButtonYes.name = radioButtonNo.name = "gender" + cont;

            labelPerg.innerHTML = questao.question;
            sessaoPerg.id = "perguntas";
           
            sessaoPerg.appendChild(labelPerg);
            sessaoPerg.appendChild(radioButtonYes);
            sessaoPerg.appendChild(radioButtonNo);
            questoes.appendChild(sessaoPerg);
            cont += 1;
       }

    } else{
        alert("Não existem perguntas.");
    }

}

btnNovaTrivia.onclick = gerarNovaTrivia;
btnFinalizarTrivia.onclick = finalizarTrivia;